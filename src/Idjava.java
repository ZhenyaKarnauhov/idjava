
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс находит все идентификаторы и записывает их в текстовый файл.
 *
 * @author Карнаухов Евгений 15ОИТ18.
 */

public class Idjava {
    public static void main(String[] args) throws IOException {
        String s;
        Pattern p = Pattern.compile("[\\W?\\w+\\W?]+");
        Matcher m = p.matcher("");
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("C:\\zadachi\\evro\\src\\evro.java"));
             BufferedWriter writer = new BufferedWriter(new FileWriter("Вывод.txt"))) {
            while ((s = bufferedReader.readLine()) != null) {
                m.reset(s);
                while (m.find()) {
                    s= m.group().replaceAll("^\\s*\\/?\\*[^\\/]*[[*]-[/]]?$","");
                    s = s.replaceAll("\\/\\/.*","");
                    s = s.replaceAll("\\s+\\s+","");
                    s = s.replaceAll("evro","Вывод");
                    writer.write(s);
                }
            }
        }
    }
}
