
import java.util.Scanner;

/**
 * Класс для демонстрации работоспособности  метода для перевода рублей в евро по заданному курсу.
 * В качестве аргументов метода передается количество рублей и курс.
 *
 * @author Карнаухов Евгений 15ОИТ18.
 */
public class evro {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Введите курс евро : ");
        double course = scanner.nextDouble();
        System.out.println("Введите количество рублей, которые хотите перевести в евро : ");
        int ruble = scanner.nextInt();
        trade(ruble, course);
        System.out.println(ruble + " руб." + " = " + String.format("%.2f", trade(ruble, course)) + " евро.");
    }

    /**
     * Метод для перевода рублей в евро по заданному курсу
     *
     * @param ruble  количество рублей
     * @param course курс евро
     * @return количество рублей в евро
     */
    public static double trade(int ruble, double course) {
        return ruble / course;
    }
}
